<?php namespace ElmhurstProjects\Core\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CoreModel extends Authenticatable
{
    use Notifiable;

    /**
     * Set the primary key from default id to uid
     * @var string
     */
    protected $primaryKey = 'uid';

    /**
     * Stop primary key auto incrementing, and being cast as int
     * @var bool
     */
    public $incrementing = false;
}
