<?php namespace ElmhurstProjects\Core\Requests;

use Carbon\Carbon;

Trait APIRequestQueryTrait
{
    /**
     * Handles basic part of building query from API query
     * @param $model - Laravel model, could be MongoDB one
     * @param $request - Laravel request
     * @return mixed
     */
    protected function APIRequestQuery($model, $request)
    {
        return $model::when($request->has('paginate'), function ($query) use ($request) {
            $paginate = json_decode(json_encode($request->get('paginate')));

            $query->skip(($paginate->page * $paginate->first));

            $query->take($paginate->first);

            return $query;
        })
            // Relationships
            ->when($request->has('relationships'), function ($query) use ($request) {
                $relationships = json_decode(json_encode($request->get('relationships')));

                foreach ($relationships as $relationship) {
                    $query->with($relationship);
                }

                return $query;
            })

            // Select
            ->when($request->has('select'), function ($query) use ($request) {
                $select = json_decode(json_encode($request->get('select')));

                return $query->select($select);
            })

            // Order
            ->when($request->has('order'), function ($query) use ($request) {
                $order = json_decode(json_encode($request->get('order')));

                return $query->orderby($order->field, $order->direction);
            })

            // Where
            ->when($request->has('params'), function ($query) use ($request) {
                $params = json_decode(json_encode($request->get('params')));

                foreach ($params as $param) {
                    if (isset($param->value) && $param->value != '%null%' && $param->value != '%%' && $param->value != null) {
                        $operator = (isset($param->operator)) ? $param->operator : '=';

                        $type = (isset($param->type)) ? $param->type : 'where';

                        switch ($type) {
                            case 'whereBetween':
                                $query->whereBetween($param->field, $param->value);

                                break;
                            case 'whereBetweenDates':
                                $query->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $param->value[0])->startOfDay())
                                    ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $param->value[1])->endOfDay())
                                    ->get();
                                break;
                            default:
                                $query->where($param->field, $operator, $param->value);

                                break;
                        }
                    }
                }

                return $query;
            });
    }
}