<?php namespace ElmhurstProjects\Core\Requests;

abstract class CoreRequest
{
    protected $request_array = [];

    /**
     * Get the value of the specified input field
     * @param string $field
     * @return mixed
     */
    public function input(string $field)
    {
        return $this->request_array[$field];
    }

    /**
     * Check to see if the request has the required fields set
     * @return bool
     */
    public function isValid(): bool
    {
        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) return false;
        }
        return true;
    }

    /**
     * get the errors if not valid
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];
        foreach ($this->required_fields as $field) {
            if ($this->request_array[$field] == null) $errors[] = 'Missing key field: ' . $field;
        }
        return $errors;
    }

    /**
     * get the errors if not valid as a string message
     * @return string
     */
    public function getErrorMessage(): string
    {
        $errors = $this->getErrors();

        $message = 'One or more of the required fields are not set: ';

        foreach ($errors as $error) {
            $message .= $error . ' ';
        }

        return $message;
    }

    /**
     * Gets the array with the requested details
     * @return array
     */
    public function getRequestArray(): array
    {
        return $this->request_array;
    }
}