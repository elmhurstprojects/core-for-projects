<?php namespace ElmhurstProjects\Core\Requests;

interface RequestInterface
{
    public function input(string $field);

    public function isValid() : bool;

    public function getErrors() : array;

    public function getErrorMessage() : string;

    public function getRequestArray() : array;
}