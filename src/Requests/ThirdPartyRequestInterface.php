<?php namespace ElmhurstProjects\Core\Requests;

interface ThirdPartyRequestInterface
{
    public function isValid():bool;
}