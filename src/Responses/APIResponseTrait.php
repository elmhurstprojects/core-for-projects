<?php namespace ElmhurstProjects\Core\Responses;

/**
 * Use with Controller
 * Trait APIResponseTrait
 * @package ElmhurstProjects\Core\Responses
 */
Trait APIResponseTrait
{
    /**
     * This standardises the API result for an API Controller
     * @param bool $success
     * @param $request
     * @param $result
     * @return mixed
     */
    protected function APIResponse(bool $success, $request, $result)
    {
        return response()->json([
            'success' => $success,
            'request' => $request->all(),
            'result' => $result
        ]);
    }
}