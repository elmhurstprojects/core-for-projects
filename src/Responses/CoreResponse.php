<?php namespace ElmhurstProjects\Core\Responses;

/**
 * Extend from this class for internal responses
 * Class CoreResponse
 * @package ElmhurstProjects\Core\Responses
 */
abstract class CoreResponse
{
    protected $successful = true;

    protected $message = null;

    /**
     * Set the response failed
     * @param string|null $message
     * @return $this
     */
    public function failed(string $message = null)
    {
        $this->successful = false;

        $this->message = $message;

        return $this;
    }

    /**
     * Set the response to successful
     * @param string|null $message
     * @return $this
     */
    public function successful(string $message = null)
    {
        $this->successful = true;

        $this->message = $message;

        return $this;
    }

    /**
     * Was response positive
     * @return bool
     */
    public function isSuccessful():bool
    {
        return $this->successful;
    }

    /**
     * Get the message
     * @return null|string
     */
    public function getMessage():? string
    {
        return $this->message;
    }
}