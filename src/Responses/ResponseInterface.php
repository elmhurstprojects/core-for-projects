<?php namespace ElmhurstProjects\Core\Responses;

interface ResponseInterface
{
    public function isSuccessful():bool;

    public function getMessage():?string;
}