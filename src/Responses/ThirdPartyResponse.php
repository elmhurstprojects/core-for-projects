<?php namespace ElmhurstProjects\Core\Responses;

/**
 * Extend this class to standardize responses from third party API providers
 * Class ThirdPartyResponse
 * @package ElmhurstProjects\Core\Responses
 */
abstract class ThirdPartyResponse
{

}