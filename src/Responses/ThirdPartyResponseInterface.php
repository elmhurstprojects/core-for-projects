<?php namespace ElmhurstProjects\Core\Responses;

interface ThirdPartyResponseInterface
{
    public function isSuccessful():bool;

    public function getError():string;

    public function getRaw():string;

    public function getRawJSON():\stdClass;
}